import subprocess

result = subprocess.run(['git', 'status'], stdout=subprocess.PIPE)

if len(result.stdout.decode('utf-8')) > 0:
    print("Please revert or commit all your changes before running the script. Aborting.")
else:
    subprocess.run(['python3', 'replacePersonalInfo.py'])
    subprocess.run(['./makeLinks.sh'])
