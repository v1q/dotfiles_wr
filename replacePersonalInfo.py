#!/usr/bin/env python3
import fileinput
import json

filesList = [
    './.gitconfig',
    './.subversion/config'
    ]

print("Please input the following information:\n")
print("OS username: ")
osUsername = input()
print("User email: ")
userEmail = input()
print("Your first name: ")
userName = input()

replaceDictionary = {
    "wikrom":osUsername,
    "wiktor.romanczuk@3cityelectronics.com":userEmail,
    "Wiktor":userName
    }

with fileinput.FileInput(filesList, inplace=True) as file:
    for line in file:
        printed = False
        for key in replaceDictionary.keys():
            if key in line:
                print(line.replace(key, replaceDictionary[key]), end='')
                printed = True
                break
        if not printed:
                print(line, end='')
